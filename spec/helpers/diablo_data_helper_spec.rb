require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the DiabloDataHelperHelper. For example:
#
# describe DiabloDataHelperHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe DiabloDataHelper, type: :helper do
  describe 'process_diablodata' do
    context 'no hero_id' do
      context 'data not exists' do
        it 'should create data' do
          expect(Career.first).to eq nil
          process_diablodata('eu', 'de_DE', 'Jimmi#2787')
          expect(Career.first).not_to eq nil
        end
      end
      context 'data exists' do
        before(:each) do
          process_diablodata('eu', 'de_DE', 'Jimmi#2787')
        end
        it 'should have data' do
          expect(Career.first).not_to eq nil
          process_diablodata('eu', 'de_DE', 'Jimmi#2787')
          expect(Career.count).to eq 1
        end
        context 'expired data' do
          it 'should update expired data' do
            expect(Career.first).not_to eq nil
            Career.first.update(updated_at: Date.today - 3.day)
            expect(Career.first.updated_at.to_date).to eq Date.today - 3.day
            process_diablodata('eu', 'de_DE', 'Jimmi#2787')
            expect(Career.first.updated_at.to_date).to eq Date.today
          end
        end
        context 'not expired data' do
          it 'should get not expired data' do
            expect(Career.first).not_to eq nil
            expect(Career.first.updated_at.to_date).to eq Date.today
          end
        end
      end
    end
    context 'with hero_id' do
      before(:each) do
        process_diablodata('eu', 'de_DE', 'Jimmi#2787')
      end
      context 'data not exists' do
        it 'should create data' do
          expect(Hero.first).to eq nil
          process_diablodata('eu', 'de_DE', 'Jimmi#2787', '8872068')
          expect(Hero.first).not_to eq nil
        end
      end
      context 'data exists' do
        before(:each) do
          process_diablodata('eu', 'de_DE', 'Jimmi#2787', '8872068')
        end
        it 'should hava data' do
          expect(Hero.first).not_to eq nil
          process_diablodata('eu', 'de_DE', 'Jimmi#2787', '8872068')
          expect(Hero.count).to eq 1
        end
        context 'expired data' do
          it 'should update expired data' do
            expect(Hero.first).not_to eq nil
            Hero.first.update(updated_at: Date.today - 3.day)
            expect(Hero.first.updated_at.to_date).to eq Date.today - 3.day
            process_diablodata('eu', 'de_DE', 'Jimmi#2787', '8872068')
            expect(Hero.first.updated_at.to_date).to eq Date.today
          end
        end
        context 'not expired data' do
          it 'should get not expired data' do
            expect(Hero.first).not_to eq nil
            expect(Hero.first.updated_at.to_date).to eq Date.today
          end
        end
      end
    end
  end
end
