Rails.application.routes.draw do
  devise_for :users
  root 'diablo#index'

  get 'diablo/search'

  get 'location/region'
  get 'location/locale'

  get 'shared/search'

  get 'career/index'

  get 'hero/index'

  get 'compare/index'
  get 'compare/career'
  get 'compare/hero'

  get 'build/index'

  get 'forum/index'

  get 'impressum/index'
  mount I18nTranslation::Engine => "/i18n_translation"
end
