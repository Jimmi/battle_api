class SharedController < ApplicationController
  protect_from_forgery only: [:create, :update, :destroy]

  def self.last_updated
    @career = Career.all.order(:updated_at).limit(10)
  end

  def search
    career = Career.battle_tag(params[:query]).select('battle_tag')
    battle_tags = []
    career.each { |c| battle_tags << c.battle_tag }
    if request.xhr?
      respond_to do |format|
        format.json { render json: battle_tags }
      end
    end
  end

end
