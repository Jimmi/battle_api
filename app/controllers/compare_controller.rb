class CompareController < ApplicationController

  def index

  end

  def career
    session[:battle_tag] = params[:battle_tag]
    session[:battle_tag2] = params[:battle_tag2]
    session[:region] = session[:region]
    session[:locale] = session[:locale]
    session[:language] = session[:locale].match(/(.*)_/)[1]
    @career = DiabloApi::Career.new(session[:region], session[:locale], params[:battle_tag])
    @career2 = DiabloApi::Career.new(session[:region], session[:locale], params[:battle_tag2])
    @portrait = DiabloApi::Icons::Portrait
  end


  def hero
    @career = DiabloApi::Career.new(session[:region], session[:locale], session[:battle_tag])
    @career2 = DiabloApi::Career.new(session[:region], session[:locale], session[:battle_tag2])
    @hero = DiabloApi::Hero.new(session[:region], session[:locale], session[:battle_tag], params[:hero_id])
    @hero2 = DiabloApi::Hero.new(session[:region], session[:locale], session[:battle_tag2], params[:hero_id2])
    @portrait = DiabloApi::Icons::Portrait
    @paperdoll = DiabloApi::Icons::Paperdoll
    @item = DiabloApi::Icons::Item
  end
end
