class LocationController < ApplicationController
  def locale
    session[:locale] = params[:locale]
    session[:language] = session[:locale].match(/(.*)_/)[1] unless session[:locale].nil?
    redirect_to :back
  end

  def region
    session[:region] = params[:region]
    redirect_to :back
  end
end
