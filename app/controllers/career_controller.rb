class CareerController < ApplicationController
  include DiabloDataHelper

  def index
    session[:battle_tag] = params[:battle_tag]
    @career = process_diablodata(session[:region], session[:locale], params[:battle_tag])
    @portrait = DiabloApi::Icons::Portrait
  end
end
