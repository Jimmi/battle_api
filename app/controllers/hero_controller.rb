class HeroController < ApplicationController
  include DiabloDataHelper

  def index
    @career = process_diablodata(session[:region], session[:locale], params[:battle_tag])
    @hero = process_diablodata(session[:region], session[:locale], params[:battle_tag], params[:hero_id])
    @portrait = DiabloApi::Icons::Portrait
    @paperdoll = DiabloApi::Icons::Paperdoll
    @item = DiabloApi::Icons::Item
    @tooltip = DiabloApi::Tooltip
  end
end
