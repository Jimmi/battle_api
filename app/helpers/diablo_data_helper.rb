module DiabloDataHelper
  def process_diablodata(region, locale, battle_tag, hero_id = nil)
    @region = region
    @locale = locale
    @battle_tag = battle_tag.downcase
    @hero_id = hero_id
    if data_exists?
      @data = data
      if data_expired?
        @data = update_model
      else
        @data = data
      end
    else
      @data = data_new
    end
  end

  private

  def data_exists?
    if @hero_id.nil?
      Career.exists?(['battle_tag ~* ?', @battle_tag])
    else
      Hero.exists?(id: @hero_id)
    end
  end

  def data
    if @hero_id.nil?
      Career.battle_tag(@battle_tag).first
    else
      Hero.find @hero_id
    end
  end

  def data_expired?
    @data.updated_at < Date.today - 1.day
  end

  def update_model
    if @hero_id.nil?
      career_to_save = api_call
      model = Career.battle_tag(@battle_tag).first
      model.battle_tag = career_to_save.data[:battle_tag]
      model.paragon_level = career_to_save.data[:paragon_level]
      model.paragon_level_hardcore = career_to_save.data[:paragon_level_hardcore]
      model.paragon_level_season = career_to_save.data[:paragon_level_season]
      model.paragon_level_season_hardcore = career_to_save.data[:paragon_level_season_hardcore]
      model.guild_name = career_to_save.data[:guild_name]
      model.heroes = career_to_save.data[:heroes]
      model.last_hero_played = career_to_save.data[:last_hero_played]
      model.last_updated = career_to_save.data[:last_updated]
      model.kills = career_to_save.data[:kills]
      model.highest_hardcore_level = career_to_save.data[:highest_hardcore_level]
      model.time_played = career_to_save.data[:time_played]
      model.progression = career_to_save.data[:progression]
      model.fallen_heroes = career_to_save.data[:fallen_heroes]
      model.blacksmith = career_to_save.data[:blacksmith]
      model.jeweler = career_to_save.data[:jeweler]
      model.mystic = career_to_save.data[:mystic]
      model.blacksmith_hardcore = career_to_save.data[:blacksmith_hardcore]
      model.jeweler_hardcore = career_to_save.data[:jeweler_hardcore]
      model.mystic_hardcore = career_to_save.data[:mystic_hardcore]
      model.blacksmith_season = career_to_save.data[:blacksmith_season]
      model.jeweler_season = career_to_save.data[:jeweler_season]
      model.mystic_season = career_to_save.data[:mystic_season]
      model.blacksmith_season_hardcore = career_to_save.data[:blacksmith_season_hardcore]
      model.jeweler_season_hardcore = career_to_save.data[:jeweler_season_hardcore]
      model.mystic_season_hardcore = career_to_save.data[:mystic_season_hardcore]
      model.seasonal_profiles = career_to_save.data[:seasonal_profiles]
      model.created_at = Time.current if model.created_at.nil?
      model.updated_at = Time.current
      model.save!
      Career.battle_tag(@battle_tag).first
    else
      hero_to_save = api_call
      hero = hero_to_save.data
      model = Hero.find(params[:hero_id])
      model.career_id = Career.battle_tag(@battle_tag).first.id
      model.name = hero[:name]
      model.hero_class = hero[:class]
      model.gender = hero[:gender]
      model.level = hero[:level]
      model.kills = hero[:kills]
      model.paragon_level = hero[:paragon_level]
      model.season_created = hero[:season_created]
      model.skills = hero[:skills]
      model.items = hero[:items]
      model.followers = hero[:followers]
      model.legendary_powers = hero[:legendary_powers]
      model.stats = hero[:stats]
      model.progression = hero[:progression]
      model.hardcore = hero[:hardcore]
      model.seasonal = hero[:seasonal]
      model.dead = hero[:dead]
      model.last_updated = hero[:last_updated]
      model.save
      model.id = hero[:id]
      model.created_at = Time.current if model.created_at.nil?
      model.updated_at = Time.current
      model.save
      Hero.find @hero_id
    end
  end

  def data_new
    if @hero_id.nil?
      career_to_save = api_call
      Career.new(career_to_save.data).save unless career_to_save.nil?
      Career.battle_tag(@battle_tag).first
    else
      hero_to_save = api_call
      hero = hero_to_save.data
      new_hero = Hero.new
      new_hero.career_id = Career.battle_tag(@battle_tag).first.id
      new_hero.name = hero[:name]
      new_hero.hero_class = hero[:class]
      new_hero.gender = hero[:gender]
      new_hero.level = hero[:level]
      new_hero.kills = hero[:kills]
      new_hero.paragon_level = hero[:paragon_level]
      new_hero.season_created = hero[:season_created]
      new_hero.skills = hero[:skills]
      new_hero.items = hero[:items]
      new_hero.followers = hero[:followers]
      new_hero.legendary_powers = hero[:legendary_powers]
      new_hero.stats = hero[:stats]
      new_hero.progression = hero[:progression]
      new_hero.hardcore = hero[:hardcore]
      new_hero.seasonal = hero[:seasonal]
      new_hero.dead = hero[:dead]
      new_hero.last_updated = hero[:last_updated]
      new_hero.save
      new_hero.id = hero[:id]
      new_hero.save
      new_hero
    end
  end

  def api_call
    if @hero_id.nil?
      respond = DiabloApi::Career.new(@region, @locale, @battle_tag)
    else
      respond = DiabloApi::Hero.new(@region, @locale, @battle_tag, @hero_id)
    end
    if respond.data[:code].nil?
      respond
    else
      if respond.data[:code].include? 'NOTFOUND'
        redirect_to(root_path, flash: { error: respond.data[:reason] }) && return
      else
        respond
      end
    end
  end
end
