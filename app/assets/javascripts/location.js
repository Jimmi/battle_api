$(function() {
  set_locale()
  set_session_locale($.cookie("locale"))
  
  if ($.cookie("region") != null) {
    target_finder = "[data-region='" + $.cookie('region') + "']";
    set_target = $('.dropdown-menu').find(target_finder).parent().parent();
    set_target.closest('.btn-group').find('[data-bind="label"]').html(set_target.html());
  }
  if ($.cookie("locale") != null) {
    target_finder = "[data-locale='" + $.cookie('locale') + "']";
    set_target = $('.dropdown-menu').find(target_finder).parent().parent();
    set_target.closest('.btn-group').find('[data-bind="label"]').html(set_target.html());
  }

  $(document.body).on('click', '#region li', function(event) {
    var $target = $(event.currentTarget);
    var dropDown = $target.closest('.btn-group').find('[data-bind="label"]').html($target.html());
    dropDown.end().children('.dropdown-toggle').dropdown('toggle');
    set_region_cookie();
    set_locale();
    set_session_region($.cookie("region"));
    return false;
  });

  $(document.body).on('click', '#locale li', function(event) {
    var $target = $(event.currentTarget);
    var dropDown = $target.closest('.btn-group').find('[data-bind="label"]').html($target.html());
    dropDown.end().children('.dropdown-toggle').dropdown('toggle');
    set_locale_cookie();
    set_session_locale($.cookie("locale"))
    return false;
  });

  function set_locale() {
    switch ($.cookie("region")) {
      case 'eu':
        $('.europe').show().attr("id", "active_location");
        $('.america').hide().removeAttr("id");
        $('.korea').hide().removeAttr("id");
        $('.taiwan').hide().removeAttr("id");
        break;
      case 'us':
        $('.europe').hide().removeAttr("id");
        $('.america').show().attr("id", "active_location");
        $('.korea').hide().removeAttr("id");
        $('.taiwan').hide().removeAttr("id");
        break;
      case 'kr':
        $('.europe').hide().removeAttr("id");
        $('.america').hide().removeAttr("id");
        $('.korea').show().attr("id", "active_location");
        $('.taiwan').hide().removeAttr("id");
        break;
      case 'tw':
        $('.europe').hide().removeAttr("id");
        $('.america').hide().removeAttr("id");
        $('.korea').hide().attr("id", "active_location");
        $('.taiwan').show().removeAttr("id");
        break;
    }

    language = $('#active_location').children().first().children();
    target = $('#current-locale');
    target.html(language.html());
  }

  function set_region_cookie() {
    region = $('#current-region').find('i');
    cookieRegion = $.cookie("region");
    if (cookieRegion !== null) {
      $.cookie("region", region.data('region'));
    } else {
      target_finder = "[data-region='" + $.cookie('region') + "']";
      set_target = $('.dropdown-menu').find(target_finder).parent().parent();
      $target.closest('.btn-group').find('[data-bind="label"]').html(set_target.html());
    }
  }

  function set_locale_cookie() {
    locale = $('#current-locale').find('i');
    cookieLocale = $.cookie("locale");
    if (cookieLocale !== null) {
      $.cookie("locale", locale.data('locale'));
    } else {
      target_finder = "[data-locale='" + $.cookie('locale') + "']";
      set_target = $('.dropdown-menu').find(target_finder).parent().parent();
      $target.closest('.btn-group').find('[data-bind="label"]').html(set_target.html());
    }
  }

  function set_session_region(region) {
    $.ajax({
        url: '/location/region',
        type: 'GET',
        dataType: 'json',
        data: {
          region: region
        }
      })
      .done(function() {
        console.log("success: set region");
      })
      .fail(function() {
        console.log("error");
      })
  }

  function set_session_locale(locale) {
    $.ajax({
      url: '/location/locale',
      type: 'GET',
      dataType: 'json',
      data: {locale: locale}
    })
    .done(function() {
      console.log("success: set locale");
    })
    .fail(function() {
      console.log("error");
    })
  }

});



function insertParam(paramName, paramValue) {
  var url = window.location.href;
  if (url.indexOf(paramName + "=") >= 0) {
    var prefix = url.substring(0, url.indexOf(paramName));
    var suffix = url.substring(url.indexOf(paramName));
    suffix = suffix.substring(suffix.indexOf("=") + 1);
    suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
    url = prefix + paramName + "=" + paramValue + suffix;
  } else {
    if (url.indexOf("?") < 0)
      url += "?" + paramName + "=" + paramValue;
    else
      url += "&" + paramName + "=" + paramValue;
  }
  window.location.href = url;
}
