namespace :diablo do
  desc "seed diablo data"
  task seed: :environment do
    include DiabloDataHelper
    battle_tags = [ 'STONEPUKE#2146', 'Jimmi#2787', 'Drahque#1198', 'Александр#21262', 'dcdead#2260', 'jahddict#2829', 'bonecrumbler#2434', 'ilovemf#2998', 'avnor#2601', 'fury#2378', 'lynk123#2590', 'bunch#2760', 'kaiser#1982', 'farky#2571', 'lemon8#2400', 'rockschwein#2801', 'madawc#2708', 'rafcior#2591', 'bloodhunter#2226', 'actionfunk#1894', 'sabre#2198', 'zeus#23805', 'tunechis#2830', 'padonak#1320' ]
    battle_tags.each do |bt|
      process_diablodata('eu', 'en_GB', bt)
      puts "#{bt} done!"
    end
  end

end
